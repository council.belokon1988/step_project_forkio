'use strict';
// gulp +
// gulp-sass +
// browser-sync +
// gulp-js-minify +
// gulp-uglify +
// gulp-clean-css +
// gulp-clean+
// gulp-concat+
// gulp-imagemin+
// gulp-autoprefixer+
//


var runSequence = require('run-sequence');
var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var minifyjs = require('gulp-js-minify');
var uglify = require('gulp-uglify-es').default;
var cleanCSS = require('gulp-clean-css');
var clean = require('gulp-clean');
var concat = require('gulp-concat');
var imagemin = require('gulp-imagemin');
var autoprefixer = require('gulp-autoprefixer');

gulp.task('dev',function () {
  browserSync.init({
    server: "./"
  });
  gulp.watch('./src/js/*.js',['minify']).on('change',browserSync.reload);
  gulp.watch('./src/scss/*.scss',['sass']).on('change',browserSync.reload);
  gulp.watch('./index.html',['img']).on('change',browserSync.reload);
});


gulp.task('sass', function () {
  return gulp.src('./src/scss/*.scss')
    .pipe(sass())
    .pipe(concat('styles.min.css'))
    .pipe(gulp.dest('./dist/css'))
});

gulp.task('img', function () {
  return gulp.src('./src/images/**')
    .pipe(imagemin({
      interlaced: true,
      svgoPlugins: [{ removeViewBox: false }],
      progressive: true,
    }))
    .pipe(gulp.dest('./dist/images'))
});


gulp.task('minify', function () {
  return gulp.src('./src/js/*.js')
    .pipe(concat('script.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./dist/js'));
});


gulp.task('clean', function () {
  return gulp.src('./dist', { read: false })
    .pipe(clean())
});

gulp.task('autoprefixer', function () {
  return gulp.src('./src/scss/*.scss')
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
});

gulp.task('build', function () {
  runSequence('clean', 'autoprefixer', 'sass', 'minify', 'img', function () {
  });
});